// In-memory component-specific store plugin

const ComponentStore = {
  emulateDelayMs: 1000,

  memoryItems: [
    { code: "1", name: "Паспорт", type: "docid"  },
    { code: "2", name: "Военный билет", type: "docid"  },
    { code: "3", name: "Свид-во о рождении", type: "docid"  },
    { code: "4", name: "Загранпаспорт", type: "docid"  },
    { code: "5", name: "Водительское", type: "docid"  },
  ],

  getItems(itemType) {
    console.log('getItems type: ' + itemType);
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        var result = this.memoryItems.filter((e) => {
          return e.type === itemType;
        });
        console.log(result);
        resolve(result);
      }, this.emulateDelayMs);
    });
  },

  install (Vue, options) {
    Vue.prototype.$componentStore = this;
  }
};

export default ComponentStore;